<?php
namespace app\components;

use Yii;
use yii\base\Widget;

class SiteMenu extends Widget
{
    public function run()
    {
        return $this->render('siteMenu');
    }
}
?>