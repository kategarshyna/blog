<?php
namespace app\components;

use yii\base\Widget;
use yii2mod\comments\models\CommentModel;

class AsideBarWidget extends Widget
{
    public function run()
    {
        $comments = CommentModel::find()->where(['status' => 1])->orderBy('createdAt desc')->limit(3)->all();

        return $this->render('asideBar', [
            'comments' => $comments
        ]);
    }
}