<a href="#" data-toggle="tooltip" data-placement="bottom" data-html="true"
   title="<p><strong>Created:</strong><?=date('l jS \of F Y h:i:s A', $model->created_at) ?></p>
        <p><strong>Updated:</strong> <?= date('l jS \of F Y h:i:s A', $model->updated_at) ?></p>
        <p><?= $model->is_published ?
    '<strong>Published:</strong>' . date('l jS \of F Y h:i:s A', $model->published_at) . '</p>'
    : '<strong>Not Published</strong></p>'
?>"><i class="fa fa-info-circle text-info"></i></a>
