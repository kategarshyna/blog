<?php
use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\UserManagementModule;

?>
<li class="sidebar-list">
<?php
echo GhostMenu::widget([
    'encodeLabels'=>false,
    'activateParents'=>true,
    'items'=> [
        ['label' => '<i class="fa fa-paperclip"></i> Posts', 'url' => ['/admin/post/index']],
        ['label' => '<i class="fa fa-list-ul"></i> Categories', 'url' => ['/admin/category/index']],
        ['label' => '<i class="fa fa-tags"></i> Tags', 'url' => ['/admin/tag/index']],
        ['label' => '<i class="fa fa-file-o"></i> Static Pages', 'url' => ['/admin/static-page/index']],
        ['label' => '<i class="fa fa-users"></i> User Control', 'url' => ['/user-management/user/index'],
            'items' => [
                ['label' => '<i class="fa fa-angle-double-right"></i> ' . UserManagementModule::t('back', 'Users'), 'url' => ['/user-management/user/index']],
                ['label' => '<i class="fa fa-angle-double-right"></i> ' . UserManagementModule::t('back', 'Roles'), 'url' => ['/user-management/role/index']],
                ['label' => '<i class="fa fa-angle-double-right"></i> ' . UserManagementModule::t('back', 'Permissions'), 'url' => ['/user-management/permission/index']],
                ['label' => '<i class="fa fa-angle-double-right"></i> ' . UserManagementModule::t('back', 'Permission groups'), 'url' => ['/user-management/auth-item-group/index']],
                ['label' => '<i class="fa fa-angle-double-right"></i> ' . UserManagementModule::t('back', 'Visit log'), 'url' => ['/user-management/user-visit-log/index']],
            ]
        ],
    ],
    'options' => [
        'class' => 'navigation navigation-main navigation-accordion navigation-my',
    ],
]);
?>
</li>

