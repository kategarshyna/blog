<?php

/** @var \yii2mod\comments\models\CommentModel[] $comments */
use app\models\Category;
use app\models\Post;
use app\models\Tag;
use yii\helpers\Url;

?>
<div class="sidebar blog-sidebar">
    <div class="sidebar-item  recent">
        <h3>Comments</h3>
        <?php foreach ($comments as $comment) : ?>
            <div class="media">
                <div class="pull-left">
                    <a href="#"></a>
                </div>
                <div class="media-body">
                    <h4><a href="<?= Url::to([Post::getPostFullUrlByEntity($comment->entityId)]) ?>">
                            <?= strlen($comment->content) > 100 ?
                                substr($comment->content, 0, 100) . '...' :
                                $comment->content ?></a></h4>
                    <p>posted <?= date('l jS \of F Y h:i A', $comment->createdAt) ?></p>
                </div>
            </div>
        <?php endforeach ?>
    </div>
    <div class="sidebar-item categories">
        <h3>Categories</h3>
        <ul class="nav navbar-stacked">
            <?php foreach (Category::getAllCategoriesList() as $category) : ?>
                <li><a href="<?= Url::to([
                        'site/category-page',
                        'categoryOrPageUrl' => $category->url
                    ]) ?>"><?= $category->name ?>
                        <span class="pull-right">(<?= $category->getPosts()->count() ?>)</span></a></li>
            <?php endforeach ?>
        </ul>
    </div>
    <div class="sidebar-item tag-cloud">
        <h3>Tag Cloud</h3>
        <ul class="nav nav-pills">
            <?php foreach (Tag::getAllTagList() as $tag) :?>
                <li><a href="<?= Url::to([
                        'site/tag',
                        'tagUrl' => $tag->url
                    ]) ?>"><?= $tag->name ?></a></li>
            <?php endforeach; ?>

        </ul>
    </div>
</div>
