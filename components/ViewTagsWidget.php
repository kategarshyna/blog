<?php
namespace app\components;

use yii\base\Widget;

class ViewTagsWidget extends Widget
{
    public $tags;

    public function run()
    {
        return $this->render('viewTags', [
            'tags' => $this->tags
        ]);
    }
}