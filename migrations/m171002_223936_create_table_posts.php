<?php

use yii\db\Migration;

class m171002_223936_create_table_posts extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%posts}}', [
            'id' => $this->integer(11)->notNull()->append('AUTO_INCREMENT PRIMARY KEY'),
            'name' => $this->string(255),
            'url' => $this->string(255),
            'description' => $this->text(),
            'is_published' => $this->smallInteger(4),
            'published_at' => $this->integer(11),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'category_id' => $this->integer(11),
            'short_content' => $this->text(),
            'content' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('FK__categories', '{{%posts}}', 'category_id', '{{%categories}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%posts}}');
    }
}
