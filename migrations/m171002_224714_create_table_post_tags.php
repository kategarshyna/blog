<?php

use yii\db\Migration;

class m171002_224714_create_table_post_tags extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%post_tags}}', [
            'post_id' => $this->integer(11)->notNull(),
            'tag_id' => $this->integer(11)->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('primary_key', '{{%post_tags}}', ['post_id','tag_id']);

        $this->addForeignKey('FK__posts', '{{%post_tags}}', 'post_id', '{{%posts}}', 'id');
        $this->addForeignKey('FK__tags', '{{%post_tags}}', 'tag_id', '{{%tags}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%post_tags}}');
    }
}
