<?php

use yii\db\Migration;

class m171002_223936_create_table_categories extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%categories}}', [
            'id' => $this->integer(11)->notNull()->append('AUTO_INCREMENT PRIMARY KEY'),
            'name' => $this->string(100),
            'url' => $this->string(100),
            'description' => $this->string(255),
            'is_published' => $this->smallInteger(4),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'published_at' => $this->integer(11),
            'parent_id' => $this->integer(11),
        ], $tableOptions);

        $this->addForeignKey('FK_categories_categories', '{{%categories}}', 'parent_id', '{{%categories}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%categories}}');
    }
}
