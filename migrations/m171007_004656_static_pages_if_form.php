<?php

use yii\db\Migration;

class m171007_004656_static_pages_if_form extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%static_pages}}', 'is_form', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%static_pages}}', 'is_form');
    }

}
