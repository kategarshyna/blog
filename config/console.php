<?php

use yii\helpers\ArrayHelper;

$config = ArrayHelper::merge(require (__DIR__ . '/common.php'), [
    'id' => 'basic-console',
    'controllerNamespace' => 'app\commands',
    'modules'=>[
        'user-management' => [
            'class' => 'webvimark\modules\UserManagement\UserManagementModule',
            'controllerNamespace'=>'vendor\webvimark\modules\UserManagement\controllers',
        ],
    ],
    'controllerMap' => [
        'migration' => [
            'class' => 'bizley\migration\controllers\MigrationController',
        ],
    ],
]);

return $config;
