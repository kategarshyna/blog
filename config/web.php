<?php

use webvimark\modules\UserManagement\models\UserVisitLog;
use yii\helpers\ArrayHelper;

$config = ArrayHelper::merge( require (__DIR__ . '/common.php') ,[
    'id' => 'basic',
    'components' => [
        'request' => [
            'cookieValidationKey' => '5cIcv6wvhC4us_UYLIOTRxGbTZZShARc',
        ],
        'user' => [
            'class' => 'webvimark\modules\UserManagement\components\UserConfig',
            'identityClass' => 'app\models\User',
            'on afterLogin' => function($event) {
                UserVisitLog::newVisitor($event->identity->id);
            }
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'class' => '\yii\web\AssetManager',
            'dirMode' => 0777,
            'fileMode' => 0666,
            'appendTimestamp' => true,
            'linkAssets' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'admin/' => 'admin/post/index',
                'site/index' => 'site/index',
                'site/contact' => 'site/contact',
                'site/captcha' => 'site/captcha',
                'site/login' => '/site/login',
                'site/logout' => 'site/logout',
                'site/profile' => 'site/profile',
                'site/register' => 'user-management/auth/registration',
                '<categoryOrPageUrl>' => 'site/category-page',
                'tags/<tagUrl>' => 'site/tag',
                '<categoryUrl>/<postUrl>' => 'site/post',
            ]
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'secret' => '6LfIeTMUAAAAACga1rWWrzUNjTcK4iF0-Ikn1vQs',
            'siteKey' => '6LfIeTMUAAAAAPpIEm2XmKa3HENC5wz7lVpY8oHX',
        ],
    ],
    'modules'=>[
        'user-management' => [
            'class' => 'webvimark\modules\UserManagement\UserManagementModule',
            'on beforeAction'=>function(yii\base\ActionEvent $event) {
                if ($event->action->uniqueId == 'user-management/auth/login') {
                    return Yii::$app->getResponse()->redirect('/site/login');
                } else {
                    $event->action->controller->layout = '@app/modules/admin/views/layouts/admin.php';
                }
                if ($event->action->uniqueId == 'user-management/auth/registration') {
                    $event->action->controller->layout = '@app/views/layouts/main.php';
                }
            }
        ],
    ],
]);

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['178.150.65.46'],
        'dirMode' => 0777,
        'fileMode' => 0666,
    ];
}

return $config;
