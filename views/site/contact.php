<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Thank you for contacting us. We will respond to you as soon as possible.
        </div>

    <?php endif; ?>

    <p>
        If you have business inquiries or other questions, please fill out the following form to contact us.
        Thank you.
    </p>

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <div class="row">
        <?= $form->field($model, 'name', ['options' => ['class' => 'col-xs-12 col-md-6']])
            ->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'phone', ['options' => ['class' => 'col-xs-12 col-md-6']])
            ->textInput([
                'class' => 'form-control',
                'id' => "phone",
                'type' => "tel",
                'value' => "+38(___)___-__-__",
                'pattern' => "^\+38(\s+)?\(?[0][0-9]{2}\)?(\s+)?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$"
            ]) ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'email', ['options' => ['class' => 'col-xs-12 col-md-6']]) ?>

        <?= $form->field($model, 'subject', ['options' => ['class' => 'col-xs-12 col-md-6']]) ?>
    </div>

    <?= $form->field($model, 'body', ['options' => ['class' => 'col-xs-12 row']])->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'reCaptcha')->widget(
        \himiklab\yii2\recaptcha\ReCaptcha::className(),
        ['siteKey' => Yii::$app->params['googleReCaptchaSiteKey']]
    ) ?>

    <div class="form-group col-xs-12 col-md-3 row">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-submit', 'name' => 'contact-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
