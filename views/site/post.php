<?php

/** @var \app\models\Post $model */

use app\models\Post;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->name;

?>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="single-blog blog-details two-column">
            <div class="post-thumb">
                <a href="#">
                    <!--                    <img src="images/blog/7.jpg" class="img-responsive" alt="">-->
                </a>
                <div class="post-overlay">
                    <span class="uppercase"><a href="#">14 <br><small>Feb</small></a></span>
                </div>
            </div>
            <div class="post-content overflow">
                <h2 class="post-title bold"><?= Html::a($model->name, [
                        'site/post',
                        'categoryUrl' => $model->category->url,
                        'postUrl' => $model->url
                    ])?></h2>
                <h3 class="post-author"><?= date('l jS \of F Y h:i:s A', $model->published_at) ?></h3>
                <p><?= $model->content ?></p>
                <div class="post-bottom overflow">
                    <ul class="nav navbar-nav post-nav">
                        <?php foreach ($model->publishedTags as $tag) : ?>
                            <li><?= Html::a('<i class="fa fa-tag"></i>' . $tag->name,
                                    ['site/tag', 'tagUrl' => $tag->url]) ?></li>
                        <?php endforeach ?>
                        <li><?= Html::a('<i class="fa fa-list-ul"></i>' . $model->category->name,
                                ['site/category-page', 'categoryOrPageUrl' => $model->category->url]) ?></li>
                        <li><a href="<?= Url::to([Post::getPostFullUrlByEntity($model->id)])?>">
                                <i class="fa fa-comments"></i><?= $model->commentsCount?> Comments</a></li>
                    </ul>
                </div>
                <div class="blog-share">
                    <span class='st_facebook_hcount'></span>
                    <span class='st_twitter_hcount'></span>
                    <span class='st_linkedin_hcount'></span>
                    <span class='st_pinterest_hcount'></span>
                    <span class='st_email_hcount'></span>
                </div>
                <?php echo \yii2mod\comments\widgets\Comment::widget([
                    'model' => $model,
                    'maxLevel' => 2,
                    'dataProviderConfig' => [
                        'pagination' => [
                            'pageSize' => 10
                        ],
                    ],
                    'listViewConfig' => [
                        'emptyText' => Yii::t('app', 'No comments found.'),
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>


