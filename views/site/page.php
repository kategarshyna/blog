<?php

/** @var \app\models\StaticPage $model */

use app\models\ContactForm;

$this->title = $model->name;
?>

<?= $model->content?>

<?php if ($model->is_form) :?>
    <?= $this->render('contact', ['model' => new ContactForm()]) ?>
<?php endif ?>
