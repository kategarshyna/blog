<?php

use yii\widgets\ListView;

/** @var \yii\debug\models\timeline\DataProvider $tagPosts */
/** @var string $tagName */

$this->title = $tagName;

?>
<h3>All posts by Tag: <strong><?= $tagName?></strong></h3>
<div class="row">
    <?= ListView::widget([
        'dataProvider' => $tagPosts,
        'pager' => [
            'hideOnSinglePage' => true,
        ],
        'layout' => '{items} <div class="blog-pagination">{pager}</div>',
        'summary'=>'',
        'itemView' => '_short_post_view'
    ])?>
</div>