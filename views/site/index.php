<?php

use yii\widgets\ListView;

$this->title = 'Blog';

/** @var \yii\debug\models\timeline\DataProvider $latestPosts */
?>

<div class="row">
    <?= ListView::widget([
        'dataProvider' => $latestPosts,
        'pager' => [
            'hideOnSinglePage' => true,
        ],
        'layout' => '{items} <div class="blog-pagination">{pager}</div>',
        'summary'=>'',
        'itemView' => '_short_post_view'
    ])?>
</div>