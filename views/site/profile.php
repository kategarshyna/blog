<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \webvimark\modules\UserManagement\models\forms\ChangeOwnPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if ($message = Yii::$app->session->getFlash('success')) :?>
    <div class="subscribe-msg  vspacer15 bspacer15 text-success">
        <?= $message ?>
    </div>
<?php endif ?>
<div class="row">
    <div class="col-md-6">
        <section>
            <h2 class="section-title">Account info</h2>
            <div class="list-group">
                <a href="#" class="list-group-item">
                    <h3>Username</h3>
                    <p><?=Yii::$app->user->identity->username ?></p>
                </a>
                <?php if (!empty(Yii::$app->user->identity->registration_ip)) :?>
                    <a href="#" class="list-group-item">
                        <h3>Registration IP</h3>
                        <p><?=Yii::$app->user->identity->registration_ip ?></p>
                    </a>
                <?php endif?>
                <a href="#" class="list-group-item">
                    <h3>Created At</h3>
                    <p><?=date('jS M, y h:i A', Yii::$app->user->identity->created_at) ?></p>
                </a>
                <a href="#" class="list-group-item">
                    <h3>Updated At</h3>
                    <p><?=date('jS M, y h:i A', Yii::$app->user->identity->updated_at) ?></p>
                </a>
            </div> <!--list-group -->
        </section>
    </div>
    <div class="col-md-6">
        <section>
            <h3 class="section-title">Update your data</h3>
            <?php $form = ActiveForm::begin([
                'id'=>'user',
                'layout'=>'horizontal',
                'validateOnBlur'=>false,
            ]); ?>

            <?php if ( $model->scenario != 'restoreViaEmail' ): ?>
                <?= $form->field($model, 'current_password')->passwordInput(['maxlength' => 255, 'autocomplete'=>'off']) ?>

            <?php endif; ?>

            <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255, 'autocomplete'=>'off']) ?>

            <?= $form->field($model, 'repeat_password')->passwordInput(['maxlength' => 255, 'autocomplete'=>'off']) ?>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <?= Html::submitButton(
                        '<span class="glyphicon glyphicon-ok"></span> Save',
                        ['class' => 'btn btn-submit']
                    ) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </section>
    </div>
</div>