<?php

use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <div class="col-md-6 col-xs-12">
        <?= $form->field($model, 'parent_id')
            ->dropDownList(ArrayHelper::map(Category::getAllCategoriesForParentList($model->id), 'id', 'name'),
                ['prompt' => 'Select parent category(optional)']) ?>

        <?= $form->field($model, 'name')
            ->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'url')
            ->textInput(['maxlength' => true])
            ->label('Url (If you leave this field blank it will be generated from Name)') ?>
    </div>
    <div class="col-md-6 col-xs-12">
        <?= $form->field($model, 'description')->textarea(['rows' => 8]) ?>
    </div>

    <div class="form-group col-xs-12">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
