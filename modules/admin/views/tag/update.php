<?php

use app\components\InfoWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tag */

$this->title = 'Update Tag: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tag-update">

    <h1><?= Html::encode($this->title) ?>  <?= $model->isNewRecord ? '' : InfoWidget::widget(['model' => $model])?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
