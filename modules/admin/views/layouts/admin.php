<?php
/* @var $content string */


use app\components\AdminMenu;
use app\modules\admin\AdminAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>

<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" href="images/ico/favicon.ico">
</head>
    <body>
    <?php $this->beginBody() ?>
        <div class="row-offcanvas full-height">
            <div id="wrapper" class="full-height">
                <div class="row full-height">
                    <div id="sidebar" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 full-height">
                        <ul class="sidebar">
                            <li class="sidebar-header">
                                <a href="/admin"><?= Yii::$app->params['siteName'] ?></a>
                            </li>
                            <?= AdminMenu::widget() ?>
                        </ul>
                        <div class="sidebar-footer">
                            <div class="col-xs-12">
                                <a href="/admin">&copy; <?= date('Y') . ' ' . Html::encode(Yii::$app->params['siteName']) ?>, LLC. All rights reserved.</a>
                            </div>
                        </div>
                    </div>
                    <div id="content" class="col-xs-12 col-sm-9 col-md-9 col-lg-9 full-height">
                        <div id="topnavbar" class="navbar navbar-default">
                            <div class="container-fluid admin-header">
                                <div class="navbar-header">
                                    <a class="btn btn-default sidebar-toggle navbar-toggle">
                                        <i class="fa fa-exchange"></i>
                                    </a>
                                    <a class="btn btn-default navbar-toggle"
                                            data-toggle="collapse"
                                            data-target="#content-navbar-collapse">
                                        <i class="fa fa-bars"></i>
                                    </a>
                                </div>
                                <div class="col-lg-10">
                                <?= Breadcrumbs::widget([
                                        'homeLink' => [
                                            'label' => Yii::t('yii', Html::encode(Yii::$app->params['siteName'])),
                                            'url' => '/admin',
                                        ],
                                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                    ]) ?>
                                </div>
                                <div id="content-navbar-collapse" class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-2x fa-user"></i>
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><p class="login-name-label">Login: <?= Yii::$app->user->identity->username ?></p></li>
                                                <li class="divider"></li>
                                                <li><a href="/user-management/auth/change-own-password">Change Own Password</a></li>
                                                <li class="divider"></li>
                                                <li><a href='/user-management/auth/logout'><i class="icon-switch2"></i> Logout</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- YOUR CONTENT GOES HERE -->

                                <?php foreach (Yii::$app->session->getAllFlashes() as $type => $message): ?>
                                    <?php if (in_array($type, ['success', 'danger', 'warning', 'info'])): ?>
                                        <div role="alert" class="alert alert-<?= $type ?> alert-dismissible top-divide-40">
                                            <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                            <?= $message ?>
                                        </div>
                                    <?php endif ?>
                                <?php endforeach ?>

                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
