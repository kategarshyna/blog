<?php

use app\components\InfoWidget;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StaticPageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Static Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="static-page-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Static Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'url',
            [
                'attribute' => 'description',
                'value' => function ($model) {
                    return $model->shortDescription;
                }
            ],
            [
                'attribute' => 'info',
                'value' => function ($model) {
                    return  InfoWidget::widget(['model' => $model]);
                },
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center']

            ],
            [
                'header' => 'Is Published',
                'value' => function ($model) {
                    if (!$model->is_published) {
                        return Html::tag('span', 'No', ['class' => "text-danger"])
                            . Html::a('Publish', ['publish-status', 'id' => $model->id], [
                                'class' => 'btn btn-xs btn-success btn-block',
                                'data-method' => 'post',
                                'data-confirm' => 'Are you sure you want to publish this item?',
                            ]);
                    } else {
                        return Html::tag('span', 'Yes', ['class' => "text-success"])
                            . Html::a('Hide', ['publish-status' ,'id' => $model->id], [
                                'class' => 'btn btn-xs btn-danger btn-block',
                                'data-method' => 'post',
                                'data-confirm' => 'Are you sure you want to hide this item?',
                            ]);
                    }
                },
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center']
            ],
            [
                'header' => 'Action',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
