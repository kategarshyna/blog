<?php

use yii\helpers\Html;
use yii\redactor\widgets\Redactor;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StaticPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="static-page-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <?= $form->field($model, 'name', ['options' => ['class' => 'col-xs-12 col-md-6']])
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url', ['options' => ['class' => 'col-xs-12 col-md-6']])
        ->textInput(['maxlength' => true])
        ->label('Url (If you leave this field blank it will be generated from Name)') ?>

    <?= $form->field($model, 'description', ['options' => ['class' => 'col-xs-12']])->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content', ['options' => ['class' => 'col-xs-12']])->widget(Redactor::className(), []) ?>

    <?= $form->field($model, 'is_form', ['options' => ['class' => 'col-xs-12']])->checkbox() ?>

    <div class="form-group col-xs-12">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ?
            'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
