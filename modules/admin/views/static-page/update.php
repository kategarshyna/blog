<?php

use app\components\InfoWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StaticPage */

$this->title = 'Update Static Page: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Static Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="static-page-update">

    <h1><?= Html::encode($this->title) ?>  <?= $model->isNewRecord ? '' : InfoWidget::widget(['model' => $model])?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
