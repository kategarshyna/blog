<?php

use app\components\InfoWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = 'Update Post: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="post-update">

    <h1><?= Html::encode($this->title) ?> <?= $model->isNewRecord ? '' : InfoWidget::widget(['model' => $model])?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
