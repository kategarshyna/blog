<?php

use app\models\Category;
use app\models\Tag;
use dosamigos\selectize\SelectizeDropDownList;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\redactor\widgets\Redactor;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="post-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <div class="col-xs-12 col-md-6">

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'url')->textInput(['maxlength' => true])
            ->label('Url (If you leave this field blank it will be generated from Name)') ?>

        <?= $form->field($model, 'category_id')
            ->dropDownList(ArrayHelper::map(Category::getAllCategoriesList(), 'id', 'name'), [
                'prompt' => 'Select post category'
            ]) ?>
    </div>
    <div class="col-xs-12 col-md-6">
        <?= $form->field($model, 'description')->textarea(['rows' => 8]) ?>
    </div>

    <?= $form->field($model, 'content', ['options' => ['class' => 'col-xs-12']])->widget(Redactor::className(), []) ?>

    <?= $form->field($model, 'short_content', ['options' => ['class' => 'col-xs-12']])
        ->widget(Redactor::className(), []) ?>

    <?= $form->field($model, 'formTags', ['options' => ['class' => 'col-xs-12']])
        ->listBox(ArrayHelper::map(Tag::getAllTagListArray(), 'id', 'name'), [
            'multiple' => true,
            'size' => 4,
            'prompt' => ''
        ])->label('Tags (To choose many items press Ctrl)') ?>

    <div class="form-group col-xs-12">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ?
            'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
