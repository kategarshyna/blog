<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\StaticPage;
use app\models\StaticPageSearch;


/**
 * StaticPageController implements the CRUD actions for StaticPage model.
 */
class StaticPageController extends AdminController
{
    public function getModelClass()
    {
        return StaticPage::class;
    }

    public function getModelSearchClass()
    {
        return StaticPageSearch::class;
    }
}
