<?php

namespace app\modules\admin\controllers;

use app\models\CategorySearch;
use Yii;
use app\models\Category;


class CategoryController extends AdminController
{

    public function getModelClass()
    {
        return Category::class;
    }

    public function getModelSearchClass()
    {
        return CategorySearch::class;
    }
}
