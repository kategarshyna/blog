<?php

namespace app\modules\admin\controllers;

use app\models\AdminBaseModel;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

abstract class AdminController extends Controller
{
    public $layout = 'admin';

    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return AdminBaseModel
     */
    abstract public function getModelClass();

    /**
     * @return AdminBaseModel
     */
    abstract public function getModelSearchClass();


    /**
     * Updates model publish status.
     * @throws Exception
     * @return mixed
     */
    public function actionPublishStatus($id)
    {
        $className = $this->getModelClass();
        if ($model = $className::findOne($id)) {
            if ($model->getIsPublished() == true) {
                $model->setIsPublished(false);
                $model->setPublishedAt(null);
            } else {
                $model->setIsPublished(true);
                $model->setPublishedAt(time());
            }
            if (!$model->save(false)) {
                throw new Exception(print_r($model->getErrors(), true));
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        $className = $this->getModelSearchClass();
        $searchModel = new $className;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $className = $this->getModelClass();
        $model = new $className;

        if (Yii::$app->request->isAjax AND $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax AND $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ActiveRecord
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $className = $this->getModelClass();

        if (($model = $className::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
