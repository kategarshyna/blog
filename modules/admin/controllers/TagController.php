<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Tag;
use app\models\TagSearch;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends AdminController
{
    public function getModelClass()
    {
        return Tag::class;
    }

    public function getModelSearchClass()
    {
        return TagSearch::class;
    }
}
