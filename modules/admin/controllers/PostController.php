<?php

namespace app\modules\admin\controllers;

use app\models\Post;
use app\models\PostSearch;
use Yii;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends AdminController
{
    public function getModelClass()
    {
        return Post::class;
    }

    public function getModelSearchClass()
    {
        return PostSearch::class;
    }
}
