<?php

namespace app\controllers;

use app\models\Category;
use app\models\Post;
use app\models\StaticPage;
use app\models\Tag;
use PHPUnit\Framework\Exception;
use webvimark\modules\UserManagement\models\forms\ChangeOwnPasswordForm;
use webvimark\modules\UserManagement\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $latestPosts = new ActiveDataProvider([
            'query' => Post::find()->where(['is_published' => true])->orderBy('published_at desc'),
            'pagination' => [
                'pagesize' => 2,
            ],]);

        return $this->render('index', [
            'latestPosts' => $latestPosts
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionCategoryPage($categoryOrPageUrl)
    {
        /** @var StaticPage $page */
        if ($page = StaticPage::find()->where(['is_published' => true])
            ->andWhere(['url' => $categoryOrPageUrl])->one()
        ) {

            if ($page->is_form) {

                $model = new ContactForm();

                if (Yii::$app->request->isAjax AND $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }

                if ($model->load(Yii::$app->request->post())) {
                    if ($model->contact(Yii::$app->params['adminEmail'])) {
                        Yii::$app->session->setFlash('contactFormSubmitted');
                        return $this->refresh();
                    } else {
                        throw new Exception(print_r($model->getErrors(), true));
                    }
                }

            }

            return $this->render('page', [
                'model' => $page
            ]);

        } elseif ($category = Category::find()
            ->where(['is_published' => true])
            ->andWhere(['url' => $categoryOrPageUrl])
            ->one()
        ) {
            /** @var Category $category */
            $categoryPosts = new ActiveDataProvider([
                'query' => $category->getPosts()->orderBy('published_at desc'),
                'pagination' => [
                    'pagesize' => 2,
                ],
            ]);

            return $this->render('category', [
                'categoryPosts' => $categoryPosts,
                'categoryName' => $category->name
            ]);
        }

        throw new NotFoundHttpException();
    }

    public function actionPost($categoryUrl, $postUrl)
    {
        if ($post = Post::find()
            ->where([Post::tableName() . '.url' => $postUrl])
            ->andWhere([Post::tableName() . '.is_published' => true])
            ->joinWith('category')
            ->andWhere([Category::tableName() . '.url' => $categoryUrl])->one()
        ) {
            return $this->render('post', [
                'model' => $post
            ]);
        }

        throw new NotFoundHttpException();
    }

    public function actionTag($tagUrl)
    {
        /** @var Tag $tag */
        if ($tag = Tag::find()->where(['url' => $tagUrl])->andWhere(['is_published' => true])->one()) {

            $tagPosts = new ActiveDataProvider([
                'query' => $tag->getPosts()->orderBy('published_at desc'),
                'pagination' => [
                    'pagesize' => 2,
                ],
            ]);

            return $this->render('tag', [
                'tagPosts' => $tagPosts,
                'tagName' => $tag->name
            ]);
        }

        throw new NotFoundHttpException();
    }

    public function actionProfile()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = User::getCurrentUser();

        if ($user->status != User::STATUS_ACTIVE) {
            throw new ForbiddenHttpException();
        }

        $model = new ChangeOwnPasswordForm(['user' => $user]);


        if (Yii::$app->request->isAjax AND $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) AND $model->changePassword()) {
            Yii::$app->session->setFlash('success', 'Password has been changed');
            return $this->refresh();
        }

        return $this->renderIsAjax('profile', compact('model'));

    }

    protected function renderIsAjax($view, $params = [])
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax($view, $params);
        } else {
            return $this->render($view, $params);
        }
    }
}
