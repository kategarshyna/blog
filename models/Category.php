<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $description
 * @property integer $is_published
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $published_at
 * @property integer $parent_id
 *
 * @property Category $parent
 * @property Category[] $categories
 * @property Post[] $posts
 * @property string $shortDescription
 */
class Category extends AdminBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_published', 'created_at', 'updated_at', 'published_at', 'parent_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['url'], 'string', 'max' => 255],
            ['url', 'unique'],
            ['url', 'match', 'pattern' => '/^[\p{L}\p{N}\v\-]+$/u'],
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' =>
                Category::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'description' => 'Description',
            'is_published' => 'Is Published',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'published_at' => 'Published At',
            'parent_id' => 'Parent Category',
        ];
    }

    public function getIsPublished()
    {
        return $this->is_published;
    }

    public function getPublishedAt()
    {
        return $this->published_at;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setIsPublished($value)
    {
        $this->is_published = $value;
    }

    public function setPublishedAt($value)
    {
        $this->published_at = $value;
    }

    public function setDescription($value)
    {
        $this->description = $value;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id'])->alias('parent');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['category_id' => 'id']);
    }

    public static function getAllCategoriesForParentList($id)
    {
        $condition = $id == null ? [] : ['<>', 'id', $id];
        return Category::find()->where($condition)->andWhere(['is_published' => true])->all();
    }

    public static function getAllCategoriesList()
    {
        return Category::find()->where(['is_published' => true])->all();
    }
}
