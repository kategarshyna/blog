<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "static_pages".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $description
 * @property string $content
 * @property integer $is_published
 * @property integer $published_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property boolean $is_form
 */
class StaticPage extends AdminBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'static_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'unique'],
            ['url', 'unique'],
            ['url', 'match', 'pattern' => '/^[\p{L}\p{N}\v\-]+$/u'],
            [['name', 'content', 'description'], 'required'],
            [['description', 'content'], 'string'],
            [['is_published', 'is_form', 'published_at', 'created_at', 'updated_at'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'description' => 'Description',
            'content' => 'Content',
            'is_published' => 'Is Published',
            'is_form' => 'Add Contact Form to this page',
            'published_at' => 'Published At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getIsPublished()
    {
        return $this->is_published;
    }

    public function getPublishedAt()
    {
        return $this->published_at;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setIsPublished($value)
    {
        $this->is_published = $value;
    }

    public function setPublishedAt($value)
    {
        $this->published_at = $value;
    }

    public function setDescription($value)
    {
        $this->description = $value;
    }
    
    public static function getAllStaticPagesList()
    {
        return StaticPage::find()->where(['is_published' => true])->all();
    }
}
