<?php

namespace app\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $subject;
    public $body;
    public $reCaptcha;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'subject', 'body'], 'required'],
            [['body'], 'string', 'min' => 10],
            [['subject'], 'string', 'min' => 2, 'max' => 100],
            ['email', 'email'],
            ['name', 'match', 'pattern' => '/^[\p{L}\s\']+$/uis'],
            ['phone', 'match', 'pattern' => '/^\+38(\s+)?\(?[0][0-9]{2}\)?(\s+)?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$/u'],
            [['reCaptcha'], ReCaptchaValidator::className(), 'secret' => Yii::$app->params['googleReCaptchaSecret'],
                'when' => function () { return !Yii::$app->request->isAjax; },
                'uncheckedMessage' => 'Please confirm that you are not a bot.']

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'reCaptcha' => 'reCaptcha',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body . "\n" . 'Phone: ' . $this->phone)
                ->send();

            return true;
        }
        return false;
    }
}
