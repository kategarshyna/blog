<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


abstract class AdminBaseModel extends ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'url',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => true,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]
        ];
    }


    abstract public function getIsPublished();

    abstract public function getPublishedAt();

    abstract public function getDescription();

    abstract public function setIsPublished($value);

    abstract public function setPublishedAt($value);

    abstract public function setDescription($value);

    public function getShortDescription()
    {
        return strlen($this->getDescription()) > 150 ? substr($this->getDescription(), 0, 150) . '...'
            : $this->getDescription();
    }
}