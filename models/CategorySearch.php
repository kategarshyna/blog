<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Category;
use yii\db\ActiveQuery;

/**
 * CategorySearch represents the model behind the search form about `app\models\Category`.
 */
class CategorySearch extends Category
{
    public $parent;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_published', 'created_at', 'updated_at', 'published_at'], 'integer'],
            [['name', 'url', 'description', 'parent'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['parent'] = [
            'asc' => [ "parent.name"=> SORT_ASC],
            'desc' => [ "parent.name"=> SORT_DESC]
        ];

        $this->load($params);

        if ($this->parent) {
            $query->innerJoinWith(['parent' => function(ActiveQuery $q) {
                $q->where(['like', 'parent.name', $this->parent]);
            }]);
        } else {
            $query->joinWith('parent');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'categories.id' => $this->id,
            'is_published' => $this->is_published,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'published_at' => $this->published_at,
        ]);

        $query->andFilterWhere(['like', 'categories.name', $this->name])
            ->andFilterWhere(['like', 'categories.description', $this->description])
            ->andFilterWhere(['like', 'categories.url', $this->url]);

        return $dataProvider;
    }
}
