<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $description
 * @property integer $is_published
 * @property integer $published_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PostTag[] $postTags
 * @property Post[] $posts
 */
class Tag extends AdminBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_published', 'published_at', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 100],
            ['name', 'unique'],
            ['url', 'unique'],
            ['url', 'match', 'pattern' => '/^[\p{L}\p{N}\v\-]+$/u'],
            [['name'], 'required'],
            [['url'], 'string', 'max' => 255],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'description' => 'Description',
            'is_published' => 'Is Published',
            'published_at' => 'Published At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getIsPublished()
    {
        return $this->is_published;
    }

    public function getPublishedAt()
    {
        return $this->published_at;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setIsPublished($value)
    {
        $this->is_published = $value;
    }

    public function setPublishedAt($value)
    {
        $this->published_at = $value;
    }

    public function setDescription($value)
    {
        $this->description = $value;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostTags()
    {
        return $this->hasMany(PostTag::className(), ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['id' => 'post_id'])->viaTable('post_tags', ['tag_id' => 'id']);
    }

    public static function getAllTagListArray()
    {
        return Tag::find()->select(['name', 'id'])->where(['is_published' => true])->asArray()->all();
    }

    public static function getAllTagList()
    {
        return Tag::find()->where(['is_published' => true])->all();
    }
}
