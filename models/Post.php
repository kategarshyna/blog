<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii2mod\comments\models\CommentModel;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $description
 * @property integer $is_published
 * @property integer $published_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $category_id
 * @property string $short_content
 * @property string $content
 *
 * @property PostTag[] $postTags
 * @property Tag[] $tags
 * @property Tag[] $publishedTags
 * @property [] $formTags
 * @property Category $category
 * @property int $commentsCount
 */
class Post extends AdminBaseModel
{
    private $formTags = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['url', 'unique'],
            ['url', 'match', 'pattern' => '/^[\p{L}\p{N}\v\-]+$/u'],
            [['name', 'content', 'short_content', 'category_id', 'description'], 'required'],
            [['description', 'content', 'short_content'], 'string'],
            [['is_published', 'published_at', 'created_at', 'updated_at', 'category_id'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
            [['short_content'], 'string', 'max' => 1000],
            [['formTags'], 'safe'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(),
                'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'description' => 'Description',
            'is_published' => 'Is Published',
            'published_at' => 'Published At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'category_id' => 'Category',
            'short_content' => 'Short Content',
            'content' => 'Content',
        ];
    }

    public function getIsPublished()
    {
        return $this->is_published;
    }

    public function getPublishedAt()
    {
        return $this->published_at;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setIsPublished($value)
    {
        $this->is_published = $value;
    }

    public function setPublishedAt($value)
    {
        $this->published_at = $value;
    }

    public function setDescription($value)
    {
        $this->description = $value;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostTags()
    {
        return $this->hasMany(PostTag::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('post_tags', ['post_id' => 'id']);
    }

    public function getPublishedTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('post_tags', ['post_id' => 'id'])
            ->andWhere(['is_published' => true]);
    }

    public function getFormTags()
    {
        return ArrayHelper::map($this->tags, 'id', 'id');
    }

    public function setFormTags($value)
    {
        $this->formTags = $value;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public static function getPostFullUrlByEntity($id)
    {
        if ($post = Post::findOne([$id])){
            return $post->category->url . '/' . $post->url;
        }
        return '#';
    }

    public function getCommentsCount()
    {
        return CommentModel::find()->where(['entityId' => $this->id])->count();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->saveTags($this->formTags);
    }


    public function saveTags($tags)
    {
        if (!empty($tags)) {
            $this->deleteTags();
            foreach (array_filter($tags) as $key => $value) {
                $new_post_tag = new PostTag();
                $new_post_tag->post_id = $this->id;
                $new_post_tag->tag_id = $value;
                if (!$new_post_tag->save()) {
                    throw new Exception(print_r($new_post_tag->getErrors(), true));
                }
            }
        }
        return true;
    }

    public function deleteTags()
    {
        foreach ($this->postTags as $tag) {
            if (!$tag->delete()) {
                throw new Exception(print_r($tag->getErrors(), true));
            }
        }
    }

}
