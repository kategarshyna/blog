<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Post;

/**
 * PostSearch represents the model behind the search form about `app\models\Post`.
 */
class PostSearch extends Post
{
    public $category;
    public $tags;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_published', 'published_at', 'created_at', 'updated_at'], 'integer'],
            [['name', 'url', 'description', 'content', 'tags', 'short_content', 'category'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['category'] = [
            'asc' => ["categories.name" => SORT_ASC],
            'desc' => ["categories.name" => SORT_DESC]
        ];

        $query->leftJoin('categories', 'categories.id = posts.category_id');

        $dataProvider->sort->attributes['tags'] = [
            'asc' => ["tags.name" => SORT_ASC],
            'desc' => ["tags.name" => SORT_DESC]
        ];

        $query->joinWith('tags');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'posts.id' => $this->id,
            'is_published' => $this->is_published,
            'published_at' => $this->published_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'posts.name', $this->name])
            ->andFilterWhere(['like', 'posts.url', $this->url])
            ->andFilterWhere(['like', 'posts.description', $this->description])
            ->andFilterWhere(['like', 'posts.short_content', $this->short_content])
            ->andFilterWhere(['like', 'posts.content', $this->content])
            ->andFilterWhere(['like', 'categories.name', $this->category])
            ->andFilterWhere(['like', 'tags.name', $this->tags]);

        return $dataProvider;
    }
}
